import {Injectable} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {ConfirmAlertComponent} from "../components/confirm-alert/confirm-alert.component";

@Injectable({
  providedIn: 'root'
})
export class ToolsService {
  constructor(private dialog: MatDialog) {
  }

  static validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  confirmationAlert(header: string, body: string, confirmString: string, afterClosed: () => any) {
    const dialogRef = this.dialog.open(ConfirmAlertComponent, {
      width: '250px',
      data: {
        text: body,
        confirmText: confirmString,
        title: header
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      afterClosed();
    });
  }
}
