import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";
import {base} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentToken: string;
  public user: any;

  constructor(private http: HttpClient) {
    this.setCurrentToken();
    this.setCurrentUser();
  }

  public getRole() {
    if (this.user) {
      return this.user.role;
    }
  }

  public checkAdmin() {
    if (this.user) {
      return this.user.role == "Admin";
    }
  }

  static logout() {
    localStorage.clear();
    location.reload();
  }

  public authenticate(email: string, password: string): Observable<any> {
    return this.http.post(base + 'users/authenticate', {
      email, password
    }).pipe(map(async (data: any) => {
      if (data) {
        await this.setLocalAuth(data);
      }
    }));
  }

  public getAuthToken(): string {
    return this.currentToken;
  }

  public register(formValues: any): Observable<any> {
    return this.http.post(base + 'users/signup', {
      password: formValues.password,
      email: formValues.email,
      role: formValues.role ? "Admin" : "User"
    })
      .pipe(map(async (data: any) => {
        if (data) {
          await this.setLocalAuth(data);
        }
      }));
  }

  private setCurrentUser() {
    if (localStorage.getItem('@restoReviews:user')) {
      this.user = JSON.parse(localStorage.getItem('@restoReviews:user'));
    }
  }

  private setCurrentToken() {
    if (localStorage.getItem('@restoReviews:access_token')) {
      this.currentToken = localStorage.getItem('@restoReviews:access_token');
    }
  }

  private async setLocalAuth(data) {
    await localStorage.setItem('@restoReviews:access_token', data.token);
    await localStorage.setItem('@restoReviews:user', JSON.stringify(data));
    await this.setCurrentUser();
    await this.setCurrentToken();
  }
}
