import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {
  HttpErrorResponse,
  HttpHandler,
  HttpHeaderResponse,
  HttpInterceptor,
  HttpProgressEvent,
  HttpRequest,
  HttpResponse,
  HttpSentEvent,
  HttpUserEvent
} from '@angular/common/http';
import {AuthService} from './auth.service';
import {base} from '../../environments/environment';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RequestInterceptorService implements HttpInterceptor {
  public exceptions: any[] = [
    {
      url: base + 'user/signup',
      method: 'POST'
    },
    {
      url: base + 'user/authenticate',
      method: 'GET'
    }
  ];

  constructor(private auth: AuthService, private router: Router) {
  }

  private addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
    if (this.doesntNeedAuth(req)) {
      return req.clone();
    }
    return req.clone({
      setHeaders: {
        Authorization: 'Bearer ' + token
      }
    });
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<| HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    if (!navigator.onLine) {
      return throwError('No Internet Connection');
    }
    return (next.handle(this.addToken(req, this.auth.getAuthToken()))
      .pipe(catchError((error: HttpErrorResponse) =>
        this.handleError(req, next, error)))) as any;
  }

  private handleError(req: HttpRequest<any>, next: HttpHandler, error: HttpErrorResponse): any {
    switch (error.status) {
      case 400:
        return throwError(error);
      case 401:
        if (req.method.toLowerCase() === 'put' && req.url === base + 'user') {
          return throwError(error);
        }
        if (req.method.toLowerCase() === 'get' && req.url === base + 'user/authenticate') {
          return throwError(error);
        }
        // return;
        return this.handle401Error(req, next);
      case 404:
        return throwError(error);
      case 409:
        return throwError(error);
      case 500:
        return throwError(error.message);
      default:
        return throwError(error);
    }
  }

  private handle401Error(req: HttpRequest<any>, next: HttpHandler): void {
    // AuthService.logout();

    // return this.auth.authenticate().pipe(map(newToken => {
    //   if (newToken) {
    //     return next.handle(this.addToken(req, newToken));
    //   }
    //   this.logoutUser();
    //   return throwError('Bad refresh token');
    // }, catchError( (err) => {
    //   console.log(err);
    //   this.logoutUser();
    //   return throwError('Bad refresh token');
    // })));
  }

  private doesntNeedAuth(req: HttpRequest<any>): boolean {
    let doesntNeedAuth = false;
    this.exceptions.forEach(ex => {
      if (req.method.toLowerCase() === ex.method.toLowerCase() && req.url.indexOf(ex.url) > -1) {
        doesntNeedAuth = true;
      }
    });
    return doesntNeedAuth;
  }
}
