import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {base} from "../../environments/environment";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ReviewService {
  public reviews = new BehaviorSubject(undefined);
  private fetching: boolean;

  constructor(private http: HttpClient) {
  }

  private getReviews() {
    return this.reviews.asObservable();
  }

  awaitData() {
    if (this.reviews.getValue() && !this.fetching) {
      this.refresh();
    }
    return this.getReviews();
  }

  refresh() {
    this.fetching = true;
    this.getAll().toPromise().then(data => {
      this.fetching = false;
      this.reviews.next(data);
    }, err => {
      this.fetching = false;
      this.reviews.error(err);
    });
  }

  getAll(): Observable<any> {
    return this.http.get(base + `reviews/`)
      .pipe(map(res => res));
  }

  getById(id: string): Observable<any> {
    return this.http.get(base + `reviews/${id}`)
      .pipe(map(res => res));
  }

  add(formValues: any) {
    return this.http.post(base + 'reviews/', {
      resto_id: formValues.resto_id,
      comments: formValues.comments,
      date: formValues.date,
      rating: formValues.rating,
    }).pipe(map((data: any) => {
      this.refresh();
      return data;
    }));
  }

  addMock(formValues: any) {
    return this.http.post(base + 'reviews/createMock', {
      resto_id: formValues.resto_id,
      comments: formValues.comments,
      date: formValues.date,
      rating: formValues.rating,
      user_id: formValues.user_id
    }).pipe(map((data: any) => {
      this.refresh();
      return data;
    }));
  }

  update(review) {
    return this.http.put(base + `reviews/${review.id}`, {
      resto_id: review.resto_id,
      comments: review.comments,
      date: review.date,
      rating: review.rating,
      user_id: review.user_id
    })
      .pipe(map((data: any) => {
        this.refresh();
        return data.body;
      }));
  }

  delete(id: string) {
    return this.http.delete(base + `reviews/${id}`)
      .pipe(map((data: any) => {
        this.refresh();
        return data.body;
      }));
  }
}
