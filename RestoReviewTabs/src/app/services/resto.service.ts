import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {base} from "../../environments/environment";
import {BehaviorSubject, Observable} from "rxjs";
import {ReviewService} from "./review.service";

@Injectable({
  providedIn: 'root'
})
export class RestoService {
  public restos = new BehaviorSubject(undefined);
  private fetching: boolean;

  constructor(private http: HttpClient, private reviewService: ReviewService) {
  }

  private getRestos() {
    return this.restos.asObservable();
  }

  awaitData() {
    if (this.restos.getValue() && !this.fetching) {
      this.refresh();
    }
    return this.getRestos();
  }

  refresh() {
    this.fetching = true;
    this.getAll().toPromise().then(data => {
      this.fetching = false;
      this.restos.next(data);
    }, err => {
      this.fetching = false;
      this.restos.error(err);
    });
  }

  getAll(): Observable<any> {
    return this.http.get(base + `restos/`)
      .pipe(map(res => res));
  }

  getById(id: string): Observable<any> {
    return this.http.get(base + `restos/${id}`)
      .pipe(map(res => res));
  }

  add(formValues: any) {
    return this.http.post(base + 'restos/', {
      name: formValues.name,
      description: formValues.description
    })
      .pipe(map((data: any) => {
        return data.body;
      }));
  }

  update(resto) {
    return this.http.put(base + `restos/${resto.id}`, {name: resto.name, description: resto.description})
      .pipe(map((data: any) => {
        this.reviewService.refresh();
        this.refresh();
        return data.body;
      }));
  }

  delete(id: string) {
    return this.http.delete(base + `restos/${id}`)
      .pipe(map((data: any) => {
        this.reviewService.refresh();
        this.refresh();
        return data.body;
      }));
  }

}
