import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {RestoService} from "./resto.service";

@Injectable()
export class RestoResolver implements Resolve<any> {

  constructor(private http: HttpClient, private restoService: RestoService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // return this.restoService.getRestoPageById(route.paramMap.get('id'));
    return this.restoService.getById(route.paramMap.get('id'));
  }

}