import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {base} from "../../environments/environment";
import {BehaviorSubject, Observable} from "rxjs";
import {ReviewService} from "./review.service";
import {RestoService} from "./resto.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public users = new BehaviorSubject(undefined);
  private fetching: boolean;

  constructor(private http: HttpClient, private reviewService: ReviewService,
              private restoService: RestoService) {
  }

  private getUsers() {
    return this.users.asObservable();
  }

  awaitData() {
    if (this.users.getValue() && !this.fetching) {
      this.refresh();
    }
    return this.getUsers();
  }

  refresh() {
    this.fetching = true;
    this.getAll().toPromise().then(data => {
      this.fetching = false;
      this.users.next(data);
    }, err => {
      this.fetching = false;
      this.users.error(err);
    });
  }

  getAll(): Observable<any> {
    return this.http.get(base + `users/`)
      .pipe(map(res => res));
  }

  getById(id: string): Observable<any> {
    return this.http.get(base + `users/${id}`)
      .pipe(map(res => res));
  }

  add(user: any) {
    return this.http.post(base + 'users', {
      email: user.email,
      password: user.password,
      role: user.role
    })
      .pipe(map((data: any) => data.body));
  }

  update(user) {
    return this.http.put(base + `users/${user.id}`, {
      email: user.email,
      password: user.password,
      role: user.role
    })
      .pipe(map((data: any) => {
        this.restoService.refresh();
        this.reviewService.refresh();
        this.refresh();
        return data.body;
      }));
  }

  delete(id: string) {
    return this.http.delete(base + `users/${id}`)
      .pipe(map((data: any) => {
        this.restoService.refresh();
        this.reviewService.refresh();
        this.refresh();
        return data.body;
      }));
  }

}
