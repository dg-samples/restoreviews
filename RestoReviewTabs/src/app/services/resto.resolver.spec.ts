import {TestBed} from '@angular/core/testing';
import {RestoResolver} from "./resto.resolver";

describe('RestoResolver', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RestoResolver = TestBed.get(RestoResolver);
    expect(service).toBeTruthy();
  });
});
