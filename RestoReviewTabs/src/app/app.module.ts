import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ComponentsModule} from "./components/components.module";
import {RestoService} from "./services/resto.service";
import {ReviewService} from "./services/review.service";
import {UserService} from "./services/user.service";
import {AuthService} from "./services/auth.service";
import {ToolsService} from "./services/tools.service";
import {RequestInterceptorService} from "./services/request-interceptor.service";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatNativeDateModule} from "@angular/material/core";
import {CustomDatePickerComponent} from "./components/custom-date-picker/custom-date-picker.component";
import {NgxInputStarRatingModule} from "ngx-input-star-rating";

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    ComponentsModule,
    MatNativeDateModule,
    NgxInputStarRatingModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    RestoService,
    ReviewService,
    UserService,
    AuthService,
    ToolsService,
    {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    {provide: HTTP_INTERCEPTORS, useClass: RequestInterceptorService, multi: true},
  ],
  exports: [
    CustomDatePickerComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {
}
