import {Component, ElementRef, forwardRef, Input, OnChanges, ViewChild} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validator} from "@angular/forms";

@Component({
  selector: 'app-custom-date-picker',
  templateUrl: './custom-date-picker.component.html',
  styleUrls: ['./custom-date-picker.component.scss'],
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => CustomDatePickerComponent),
      multi: true
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CustomDatePickerComponent),
      multi: true
    }
  ]
})
export class CustomDatePickerComponent implements OnChanges, ControlValueAccessor, Validator {

  @ViewChild('app-custom-date-picker', {read: ElementRef, static: false}) inputElement: ElementRef;

  _formControl = new FormControl();

  @Input() label = 'Choose a date';
  @Input() required? = false;
  @Input() disabled? = false;
  @Input() autofocus? = false;
  @Input() allowFutureDates? = true;

  maxDate;

  innerValue;

  onChange: any = () => {
  };
  onTouched: any = () => {
  };

  focus() {
    this.inputElement.nativeElement.focus();
  }

  ngOnChanges() {
    this.maxDate = this.allowFutureDates ? null : new Date();
  }

  writeValue(value: any) {
    this.innerValue = value;
    this._formControl.setValue(this.innerValue);
  }

  // This needs to be wired to the dateChange event and not dateInput event so that
  // the changed value is only bubbled up when the user changes focus or selects from
  // the popup calendar and not on each key stroke
  onDateChange(event) {
    this.onChange(event.value);
  }

  registerOnChange(fn: (value: any) => void) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
    if (isDisabled) {
      this._formControl.disable();
    } else {
      this._formControl.enable();
    }
  }

  validate(control: FormControl) {
    const errors = Object.assign({}, this._formControl.errors || {});
    return Object.keys(errors).length && this._formControl.invalid ? errors : null;
  }

  onBlur($event) {
    if ($event.target && $event.target.value && $event.target.value.length === 8 && !isNaN($event.target.value)) {
      const val: String = $event.target.value;
      const month = val.slice(0, 2);
      const day = val.slice(2, 4);
      const year = val.slice(4);
      this.innerValue = new Date(`${month}/${day}/${year}`);
      this._formControl.setValue(this.innerValue);
      this._formControl.updateValueAndValidity();
      this.onChange(this.innerValue);
    }
    if (this._formControl.hasError('matDatepickerParse')) {
      this._formControl.setValue(null);
      this._formControl.updateValueAndValidity();
    }

    this.onTouched();
  }
}
