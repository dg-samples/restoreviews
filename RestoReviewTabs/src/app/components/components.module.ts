import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ConfirmAlertComponent} from './confirm-alert/confirm-alert.component';
import {MatButtonModule, MatInputModule, MatSelectModule} from '@angular/material';
import {MaterialModule} from "../material.module";
import {MatFormFieldModule} from "@angular/material/form-field";
import {CustomDatePickerComponent} from "./custom-date-picker/custom-date-picker.component";

@NgModule({
  declarations: [
    ConfirmAlertComponent,
    CustomDatePickerComponent,

  ],
  entryComponents: [
    ConfirmAlertComponent,
    CustomDatePickerComponent,

  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MaterialModule,
    MatFormFieldModule,
    ReactiveFormsModule,
  ],
  exports: [
    ConfirmAlertComponent,
    CustomDatePickerComponent,

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule {
}
