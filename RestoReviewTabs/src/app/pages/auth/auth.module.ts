import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {AuthPage} from './auth.page';
import {LoginPageModule} from "../login/login.module";
import {RegisterPageModule} from "../register/register.module";
import {MaterialModule} from "../../material.module";

const routes: Routes = [
  {
    path: '',
    component: AuthPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    LoginPageModule,
    RegisterPageModule,
    MaterialModule
  ],
  declarations: [AuthPage],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AuthPageModule {
}
