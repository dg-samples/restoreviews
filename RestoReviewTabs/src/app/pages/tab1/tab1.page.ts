import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {RestoService} from "../../services/resto.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ToolsService} from "../../services/tools.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ReviewService} from "../../services/review.service";

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  public restos: any[];
  public displayedColumns: string[] = ['name', 'description', 'buttons'];
  public dataSource: MatTableDataSource<any>;
  public editing: any = {};
  public addNew = false;
  public newForm: FormGroup;

  constructor(private restoService: RestoService, private dialog: MatDialog,
              private fb: FormBuilder, private tools: ToolsService,
              private cd: ChangeDetectorRef, private snackBar: MatSnackBar,
              private reviewService: ReviewService) {
  }

  ngOnInit() {

    this.restoService.getAll().subscribe(data => {
      this.restos = data;
      this.dataSource = new MatTableDataSource(this.restos);
      this.refreshList();
    });

    this.reviewService.awaitData().subscribe(data => this.refreshList());

    this.newForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', Validators.required)
    });
  }

  edit(element) {
    this.editing = JSON.parse(JSON.stringify(element));
  }

  cancelEdit() {
    this.editing = {};
  }

  add() {
    this.addNew = true;
  }

  cancelAdd() {
    this.addNew = false;
    this.newForm.reset();
  }

  saveNew(formValues) {
    this.restoService.add(formValues).subscribe(res => {
      this.refreshList();
      this.cancelAdd();
      this.restoService.refresh();
      this.snackBar.open('New restaurant added.', 'Ok', {});
    }, err => {
      this.tools.confirmationAlert('Error', err.error.message, 'Ok', () => {
      });
    });
  }

  update(resto) {
    this.restoService.update(resto).toPromise()
      .then(data => {
        this.snackBar.open('Restaurant updated.', 'Ok', {});
        this.cancelEdit();
        this.refreshList();
        this.restoService.refresh();
      })
      .catch(err => {
        this.tools.confirmationAlert('Error', 'We could not update the restaurant at this time.', "Ok", () => {
        });
      });
  }

  delete(id: any) {
    this.restoService.delete(id).toPromise()
      .then(data => {
        this.snackBar.open('Restaurant deleted.', 'Ok', {});
        this.restoService.refresh();
        this.refreshList();
      })
      .catch(err => {
        this.tools.confirmationAlert('Error', 'We could not delete the restaurant at this time.', "Ok", () => {
        });
      });
  }

  refreshList() {
    this.restoService.getAll().toPromise().then(data => this.dataSource.data = data);
  }
}
