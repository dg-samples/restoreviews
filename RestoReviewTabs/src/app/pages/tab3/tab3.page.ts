import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {ToolsService} from "../../services/tools.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {UserService} from "../../services/user.service";


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  public users: any[] = [];
  public displayedColumns: string[] = ['email', 'password', 'role', 'buttons'];
  public dataSource: MatTableDataSource<any>;
  public editing: any = {};
  public addNew = false;
  public newForm: FormGroup;
  public roles = [
    {
      id: 'Admin',
      name: "Admin"
    }, {
      id: 'User',
      name: 'User'
    }
  ];

  constructor(private dialog: MatDialog, private userService: UserService,
              private fb: FormBuilder, private tools: ToolsService,
              private cd: ChangeDetectorRef, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.userService.getAll().subscribe(data => {
      this.users = data;
      this.dataSource = new MatTableDataSource(this.users);
      this.refreshList();
    });

    this.newForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      role: new FormControl('', Validators.required),
    });
  }

  edit(element) {
    this.editing = JSON.parse(JSON.stringify(element));
  }

  cancelEdit() {
    this.editing = {};
  }

  add() {
    this.addNew = true;
  }

  cancelAdd() {
    this.addNew = false;
    this.newForm.reset();
  }

  saveNew(formValues) {
    this.userService.add(formValues).subscribe(res => {
      this.refreshList();
      this.cancelAdd();
      this.userService.refresh();
      this.snackBar.open('New user added.', 'Ok', {});
    }, err => {
      console.log(err);
      this.tools.confirmationAlert('Error', err.error.message, 'Ok', () => {
      });
    });
  }

  async update(user) {
    let errors = await this.validateUser(user);
    if (errors.length) {
      await this.tools.confirmationAlert('Error', `Please fill out fields: ${errors}.`, "Ok", () => {
      });
      return;
    }
    this.userService.update(user).toPromise()
      .then(data => {
        this.snackBar.open('User updated.', 'Ok', {});
        this.cancelEdit();
        this.userService.refresh();
        this.refreshList();
      })
      .catch(err => {
        console.log(err);
        this.tools.confirmationAlert('Error', 'We could not update the user at this time.', "Ok", () => {
        });
      });
  }

  delete(id: any) {
    this.userService.delete(id).toPromise()
      .then(data => {
        this.snackBar.open('User deleted.', 'Ok', {});
        this.refreshList();
        this.userService.refresh();
      })
      .catch(err => {
        console.log(err);
        this.tools.confirmationAlert('Error', 'We could not delete the user at this time.', "Ok", () => {
        });
      });
  }

  refreshList() {
    this.userService.getAll().toPromise().then(data => this.dataSource.data = data);
  }

  private validateUser(user: any) {
    const fields = [];
    if (!user.password) {
      fields.push('password');
    }
    if (!user.email) {
      fields.push('email');
    }
    if (!user.role) {
      fields.push('role');
    }
    return fields;
  }
}
