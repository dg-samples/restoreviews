import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ReviewService} from "../../services/review.service";
import {ToolsService} from "../../services/tools.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {RestoService} from "../../services/resto.service";
import {AuthService} from "../../services/auth.service";
import {UserService} from "../../services/user.service";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";

@Component({
  selector: 'app-resto',
  templateUrl: './resto.page.html',
  styleUrls: ['./resto.page.scss'],
})
export class RestoPage implements OnInit, OnDestroy {
  public data: any;
  public newForm: FormGroup;
  public submitted = false;
  private _endSubs: Subject<any> = new Subject();
  private endSubs$ = this._endSubs.asObservable();

  constructor(private route: ActivatedRoute, private reviewService: ReviewService,
              private tools: ToolsService, private snackBar: MatSnackBar,
              private restoService: RestoService, private auth: AuthService,
              private userService: UserService, private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    console.log(this.route.snapshot.data[0]);
    this.data = this.route.snapshot.data[0];
    console.log(this.data);
    this.newForm = new FormGroup({
      date: new FormControl('', [Validators.required]),
      comments: new FormControl('', Validators.required),
      rating: new FormControl('', Validators.required)
    });

    this.reviewService.awaitData().pipe(takeUntil(this.endSubs$)).subscribe(data => {
      this.refreshList();
    });
    this.restoService.awaitData().pipe(takeUntil(this.endSubs$)).subscribe(data => {
      this.refreshList();
    });
    this.userService.awaitData().pipe(takeUntil(this.endSubs$)).subscribe(data => {
      this.refreshList();
    });
  }

  ngOnDestroy(): void {
    this._endSubs.next();
  }

  saveNew(formValues) {
    formValues.resto_id = this.data.resto.id;
    this.reviewService.add(formValues).toPromise().then(res => {
      this.submitted = true;
      this.newForm.reset();
      this.snackBar.open('Thank you for your review!', 'Ok', {});
    }, err => {
      console.log(err);
      this.tools.confirmationAlert('Error', err.error.message, 'Ok', () => {
      });
    });
  }

  refreshList() {
    this.restoService.getById(this.data.resto.id).toPromise().then(data => {
      this.data = data;
      this.cd.detectChanges();
    });
  }
}
