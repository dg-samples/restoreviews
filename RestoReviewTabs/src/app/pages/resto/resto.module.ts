import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {RestoPage} from './resto.page';
import {MatIconModule} from "@angular/material/icon";
import {MaterialModule} from "../../material.module";
import {MatListModule} from "@angular/material/list";
import {ComponentsModule} from "../../components/components.module";
import {NgxInputStarRatingModule} from "ngx-input-star-rating";

const routes: Routes = [
  {
    path: '',
    component: RestoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    MatIconModule,
    MaterialModule,
    MatListModule,
    ComponentsModule,
    ReactiveFormsModule,
    NgxInputStarRatingModule
  ],
  declarations: [RestoPage]
})
export class RestoPageModule {
}
