import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {ToolsService} from "../../services/tools.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  @Output() login: EventEmitter<any> = new EventEmitter();
  public regForm: FormGroup;
  public submitted = false;
  public errorNames: string[] = [];
  public returnUrl: string;

  constructor(private auth: AuthService, private tools: ToolsService,
              private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.regForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      rePassword: new FormControl('', [Validators.required]),
      role: new FormControl(''),
    }, {validators: this.checkPasswords});

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'home';

  }

  public register(values) {
    this.submitted = true;
    if (this.validateReg(values)) {
      this.auth.register(values).subscribe(res => {
        this.handleSuccessAuth(res);
        console.log(res);
      }, err => {
        console.log(err);
        this.tools.confirmationAlert('Error', err.error.message, 'Ok', () => {
        });
      });
    }
  }

  public logIn() {
    this.login.emit();
  }

  private validateReg(data) {
    this.errorNames = [];
    if (data.email === '' || !ToolsService.validateEmail(data.email)) {
      this.errorNames.push('email');
    }
    if (data.password.length < 6) {
      this.errorNames.push('password');
    }
    if (data.rePassword == '' || data.password !== data.rePassword) {
      this.errorNames.push('rePassword');
    }
    return this.errorNames.length === 0;
  }

  private checkPasswords(group: FormGroup) {
    const condition = group.controls['password'].value === group.controls['rePassword'].value;
    return condition ? null : {notSame: true};

  }

  private async handleSuccessAuth(res: any) {
    await this.router.navigate([this.returnUrl]);

  }

}
