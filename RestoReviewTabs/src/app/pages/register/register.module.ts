import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {RegisterPage} from './register.page';
import {MaterialModule} from "../../material.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  exports: [
    RegisterPage
  ],
  declarations: [RegisterPage]
})
export class RegisterPageModule {
}
