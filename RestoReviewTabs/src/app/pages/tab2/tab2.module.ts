import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Tab2Page} from './tab2.page';
import {MaterialModule} from "../../material.module";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {ComponentsModule} from "../../components/components.module";
import {NgxInputStarRatingModule} from "ngx-input-star-rating";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{path: '', component: Tab2Page}]),
    ReactiveFormsModule,
    MaterialModule,
    MatDatepickerModule,
    ComponentsModule,
    NgxInputStarRatingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [Tab2Page]
})
export class Tab2PageModule {
}
