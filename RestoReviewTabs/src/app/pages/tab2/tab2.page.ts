import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {ToolsService} from "../../services/tools.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ReviewService} from "../../services/review.service";
import {RestoService} from "../../services/resto.service";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  public restos: any[] = [];
  public reviews: any[] = [];
  public users: any[] = [];
  public displayedColumns: string[] = ['resto_id', 'date', 'name', 'comments', 'rating', 'buttons'];
  public dataSource;
  public editing: any = {};
  public addNew = false;
  public newForm: FormGroup;

  constructor(private reviewService: ReviewService, private dialog: MatDialog,
              private fb: FormBuilder, private tools: ToolsService,
              private cd: ChangeDetectorRef, private snackBar: MatSnackBar,
              private restoService: RestoService, private userService: UserService) {
  }

  ngOnInit() {
    this.reviewService.getAll().subscribe(data => {
      this.reviews = data;
      this.dataSource = new MatTableDataSource(this.reviews);
      this.refreshList();
    });

    this.userService.awaitData().subscribe(data => {
      this.refreshList();
      this.users = data;
    });
    this.restoService.awaitData().subscribe(data => {
      this.refreshList();
      this.restos = data;
    });
    this.reviewService.awaitData().subscribe(data => {
      if (this.dataSource) {
        this.dataSource.data = data;
      }
    });

    this.restoService.getAll().subscribe(data => {
      this.restos = data;
    });

    this.userService.getAll().subscribe(data => {
      this.users = data;
    });

    this.newForm = new FormGroup({
      resto_id: new FormControl('', [Validators.required]),
      date: new FormControl('', Validators.required),
      comments: new FormControl('', Validators.required),
      rating: new FormControl('', Validators.required),
      user_id: new FormControl('', Validators.required),
    });
  }

  edit(element) {
    this.editing = JSON.parse(JSON.stringify(element));
  }

  cancelEdit() {
    this.editing = {};
  }

  add() {
    this.addNew = true;
  }

  cancelAdd() {
    this.addNew = false;
    this.newForm.reset();
  }

  saveNew(formValues: any) {
    this.reviewService.addMock(formValues).subscribe(res => {
      this.refreshList();
      this.cancelAdd();
      this.reviewService.refresh();
      this.snackBar.open('New review added.', 'Ok', {});
    }, err => {
      console.log(err);
      this.tools.confirmationAlert('Error', err.error.message, 'Ok', () => {
      });
    });
  }

  update(review: any) {
    // console.log(review);
    this.reviewService.update(review).toPromise()
      .then(data => {
        this.snackBar.open('Review updated.', 'Ok', {});
        this.cancelEdit();
        this.refreshList();
        this.reviewService.refresh();
        this.restoService.refresh();
      })
      .catch(err => {
        console.log(err);
        this.tools.confirmationAlert('Error', 'We could not update the review at this time.', "Ok", () => {
        });
      });
  }

  delete(id: any) {
    this.reviewService.delete(id).toPromise()
      .then(data => {
        this.snackBar.open('Review deleted.', 'Ok', {});
        this.refreshList();
        this.reviewService.refresh();
        this.restoService.refresh();
      })
      .catch(err => {
        console.log(err);
        this.tools.confirmationAlert('Error', 'We could not delete the review at this time.', "Ok", () => {
        });
      });
  }

  refreshList() {
    this.reviewService.getAll().toPromise().then(data => this.dataSource.data = data);
  }
}
