import {Component, OnInit} from '@angular/core';
import {RestoService} from "../../services/resto.service";
import {MatTableDataSource} from "@angular/material/table";
import {ReviewService} from "../../services/review.service";

@Component({
  selector: 'app-feed',
  templateUrl: './feed.page.html',
  styleUrls: ['./feed.page.scss'],
})
export class FeedPage implements OnInit {
  public restos: any[] = [];
  public dataSource: MatTableDataSource<any>;
  public displayedColumns: string[] = ['resto', 'avg_rating'];

  constructor(private restoService: RestoService, private reviewService: ReviewService) {
  }

  ngOnInit() {
    this.restoService.getAll().subscribe(data => {
      this.restos = data;
      this.dataSource = new MatTableDataSource(this.restos);
      this.refreshList();
    });
    this.reviewService.awaitData().subscribe(data => this.refreshList());
    this.restoService.awaitData().subscribe(data => this.refreshList());
  }

  refreshList() {
    this.restoService.getAll().toPromise().then(data => this.dataSource.data = data);
  }

}
