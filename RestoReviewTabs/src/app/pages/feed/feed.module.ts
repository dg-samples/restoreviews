import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {FeedPage} from './feed.page';
import {MaterialModule} from "../../material.module";
import {RestoResolver} from "../../services/resto.resolver";
import {NgxInputStarRatingModule} from "ngx-input-star-rating";

const routes: Routes = [
  {
    path: '',
    component: FeedPage,
  },
  {
    path: ':id',
    loadChildren: () =>
      import('../resto/resto.module').then(m => m.RestoPageModule),
    resolve: [RestoResolver]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaterialModule,
    RouterModule.forChild(routes),
    NgxInputStarRatingModule
  ],
  declarations: [FeedPage],
  providers: [RestoResolver]
})
export class FeedPageModule {
}
