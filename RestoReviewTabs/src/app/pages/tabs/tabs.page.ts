import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {
  admin = false;
  role = '';
  links = [
    {
      name: "FEED",
      url: "/feed",
      icon: "home",
      page: "feed",
      role: "User"
    },
    {
      name: "RESTOS",
      url: "/resto-list-admin",
      icon: "restaurant",
      page: "tab1",
      role: "Admin"
    },
    {
      name: "REVIEWS",
      url: "/review-list-admin",
      icon: "create",
      page: "tab2",
      role: "Admin"
    },
    {
      name: "USERS",
      url: "/user-list-admin",
      icon: "people",
      page: "tab3",
      role: "Admin"
    }
  ];

  constructor(private auth: AuthService) {
  }

  ngOnInit(): void {
    this.role = this.auth.getRole();
    this.admin = this.role == "Admin";
  }

  checkRole(tab: any) {
    if (tab.role) {
      return this.getRole() == tab.role;
    }
  }

  getRole() {
    return this.auth.getRole();
  }

  logOut() {
    AuthService.logout();
  }

}
