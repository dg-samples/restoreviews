import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ToolsService} from "../../services/tools.service";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  @Output() register: EventEmitter<any> = new EventEmitter();
  public loginForm: FormGroup;
  public submitted = false;
  public errorNames: string[] = [];
  public returnUrl: string;

  constructor(private auth: AuthService, private tools: ToolsService,
              private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required)
    });

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'home';
  }

  public signUp() {
    this.register.emit();
  }

  public async login(data) {
    this.submitted = true;
    if (this.validatedLogin(data)) {
      await this.auth.authenticate(data.email, data.password).toPromise().then(async res => {
        await this.handleSuccessAuth(res);
      }, err => {
        console.log(err);
        this.tools.confirmationAlert('Error', err.error.message, 'Ok', () => {
        });
      });
    }
  }

  private validatedLogin(data) {
    this.errorNames = [];
    if (data.email === '' || !ToolsService.validateEmail(data.email)) {
      this.errorNames.push('email');
    }
    if (data.password === '') {
      this.errorNames.push('password');
    }
    return this.errorNames.length === 0;
  }

  private async handleSuccessAuth(res: any) {
    await this.router.navigate([this.returnUrl]);

  }

}
