import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {LoginPage} from './login.page';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MaterialModule} from "../../material.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatFormFieldModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  exports: [
    LoginPage
  ],
  declarations: [LoginPage]
})
export class LoginPageModule {
}
