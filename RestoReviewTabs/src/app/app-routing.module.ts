import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthRouteGuardService} from "./services/auth-route-guard.service";
import {AuthRedirectGuardService} from "./services/auth-redirect-guard.service";

const routes: Routes = [
  {
    path: '',
    loadChildren: './pages/tabs/tabs.module#TabsPageModule',
    canActivate: [AuthRouteGuardService]
  },
  {
    path: 'home',
    loadChildren: './pages/tabs/tabs.module#TabsPageModule',
    canActivate: [AuthRouteGuardService]
  },
  {
    path: 'auth',
    loadChildren: './pages/auth/auth.module#AuthPageModule',
    canActivate: [AuthRedirectGuardService]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      onSameUrlNavigation: 'reload',
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
