# RestoReviews

A simple restaurant review application with user and admin roles.

Node/Express API using:
* [Babel](https://www.npmjs.com/package/@babel/core) to transpile ES6 into ES5
* [bcrypt](https://www.npmjs.com/package/bcrypt) for password encryption and management
* [sqlite3](https://www.npmjs.com/package/sqlite3) for testing database ([mysql](https://www.npmjs.com/package/mysql) in production)
* [uuid](https://www.npmjs.com/package/uuid) to generate unique string identifiers
* [express-jwt](https://www.npmjs.com/package/express-jwt) and [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)

Angular/Ionic/TypeScript Front End Application using:
* [@angular/core](https://www.npmjs.com/package/@angular/core)
* [@angular/flex-layout](https://www.npmjs.com/package/@angular/flex-layout) to use flex-layout directives and flex-box styling shortcuts
* [@angular/forms](https://www.npmjs.com/package/@angular/forms)
* [@angular/material](https://www.npmjs.com/package/@angular/material)
* [rxjs](https://www.npmjs.com/package/rxjs)