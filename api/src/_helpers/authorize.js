const expressJwt = require('express-jwt');
const {secret} = require('../config.json');

module.exports = authorize;

function authorize(roles = []) {

	if (typeof roles === 'string') {
		roles = [roles];
	}

	return [
		// authenticate JWT token and attach user to request object (req.user)
		expressJwt({secret}),

		// authorize based on user role
		(req, res, next) => {
			// console.log(req.user.role);
			// console.log(roles);
			// console.log(roles.includes(req.user.role));
			if (roles.length >= 1 && !roles.includes(req.user.role)) {
				// user's role is not authorized
				return res.status(401).json({message: 'Unauthorized'});
			}
			// authentication and authorization successful
			next();
		}
	];
}