import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

const errorHandler = require('./_helpers/error-handler');
const AppDAO = require('./database/dao');
const UserRepository = require('./database/user_repository');
const RestoRepository = require('./database/resto_repository');
const ReviewRepository = require('./database/review_repository');

//create startup tables
const dao = new AppDAO('./chinook.db');
export const userRepo = new UserRepository(dao);
export const restoRepo = new RestoRepository(dao);
export const reviewRepo = new ReviewRepository(dao);

const start = async () => {
	try {
		await userRepo.createTable();
		await restoRepo.createTable();
		await reviewRepo.createTable();
	} catch (err) {
		console.log(err);
	}
};

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors());

// api routes
app.route('/').get((req, res, next) => res.json({"RUNNING": "SERVER"}));
app.use('/users', require('../src/controllers/user.controller'));
app.use('/restos', require('../src/controllers/resto.controller'));
app.use('/reviews', require('../src/controllers/review.controller'));

// global error handler
app.use(errorHandler);

const port = process.env.NODE_ENV === 'production' ? 80 : 5000;
//listen to specified port
app.listen(port, async () => {
	console.log(`server running on port ${port}`);
	try {
		await start();
	} catch (err) {
		console.log(err);
	}
});