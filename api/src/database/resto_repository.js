const uuidv4 = require('uuid/v4');
const table = "restos";

class RestoRepository {
	constructor(dao) {
		this.dao = dao
	}

	createTable() {
		const sql = `
      CREATE TABLE IF NOT EXISTS ${table} (
        id TEXT PRIMARY KEY,
        name TEXT,
        description TEXT,
        top_review_id TEXT,
        bottom_review_id TEXT,
        recent_review_id TEXT,
        avg_rating FLOAT)`;
		return this.dao.run(sql);
	}

	create(name, description) {
		const uuid = uuidv4().toString();
		return this.dao.run(
			`INSERT INTO ${table} (id, name, description) VALUES (?, ?, ?)`,
			[uuid, name, description]);
	}

	update(id, name, description) {
		return this.dao.run(
			`UPDATE ${table} 
			SET name = ?,
			 description = ?
		 WHERE id = ?`,
			[name, description, id]
		);
	}

	updateReviews(id, top, bottom, recent, avg) {
		return this.dao.run(
			`UPDATE ${table} 
			SET top_review_id = ?,
        bottom_review_id = ?,
        recent_review_id = ?,
        avg_rating = ?
		 WHERE id = ?`,
			[top, bottom, recent, avg, id]
		);
	}

	delete(id) {
		return this.dao.run(
			`DELETE FROM ${table} WHERE id = ?`,
			[id]
		)
	}

	getById(id) {
		return this.dao.get(
			`SELECT * FROM ${table} WHERE id = ?`,
			[id])
	}

	getAll() {
		return this.dao.all(`SELECT * FROM ${table} ORDER BY avg_rating DESC`)
	}
}

module.exports = RestoRepository;