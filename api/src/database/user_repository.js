const uuidv4 = require('uuid/v4');
const table = "users";

class UserRepository {
	constructor(dao) {
		this.dao = dao
	}

	createTable() {
		const sql = `
      CREATE TABLE IF NOT EXISTS ${table} (
        id TEXT NOT NULL PRIMARY KEY,
        email TEXT NOT NULL UNIQUE,
        password TEXT NOT NULL,
        role TEXT NOT NULL,
        CONSTRAINT UC_User UNIQUE (id, email))`;
		return this.dao.run(sql)
	}

	create(email, password, role) {
		const uuid = uuidv4().toString();
		return this.dao.run(
			`INSERT INTO ${table} (id, email, password, role) VALUES (?, ?, ?, ?)`,
			[uuid, email, password, role]);
	}

	update(id, email, password, role) {
		return this.dao.run(
			`UPDATE ${table} 
			SET email = ?,
			 password = ?,
			 role = ?
		 WHERE id = ?`,
			[email, password, role, id]
		);
	}

	delete(id) {
		return this.dao.run(
			`DELETE FROM ${table} WHERE id = ?`,
			[id]
		)
	}

	getById(id) {
		return this.dao.get(
			`SELECT * FROM ${table} WHERE id = ?`,
			[id])
	}

	getByEmail(email) {
		return this.dao.get(
			`SELECT * FROM ${table} WHERE email = ?`,
			[email])
	}

	getAll() {
		return this.dao.all(`SELECT email, id, role FROM ${table}`);
	}
}

module.exports = UserRepository;