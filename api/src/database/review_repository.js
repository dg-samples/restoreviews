const uuidv4 = require('uuid/v4');
const table = "reviews";
const userTable = "users";
const restoTable = "restos";


class ReviewRepository {
	constructor(dao) {
		this.dao = dao
	}

	createTable() {
		const sql = `
      CREATE TABLE IF NOT EXISTS ${table} (
        id TEXT PRIMARY KEY,
        resto_id TEXT,
        comments TEXT,
        date DATE,
        rating INT,
        user_id TEXT,
        CONSTRAINT reviews_fk_resto_id FOREIGN KEY (resto_id)
          REFERENCES restos(id) ON UPDATE CASCADE ON DELETE CASCADE
        CONSTRAINT reviews_fk_user_id FOREIGN KEY (user_id)
          REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE)`;
		return this.dao.run(sql)
	}

	create(resto_id, comments, date, rating, user_id) {
		const uuid = uuidv4().toString();
		return this.dao.run(
			`INSERT INTO ${table} (id, resto_id, comments, date, rating, user_id) VALUES (?, ?, ?, ?, ?, ?)`,
			[uuid, resto_id, comments, date, rating, user_id]);
	}

	update(id, resto_id, comments, date, rating) {
		return this.dao.run(
			`UPDATE ${table} 
			SET resto_id = ?,
			 comments = ?,
			 date = ?,
			 rating = ?
		 WHERE id = ?`,
			[resto_id, comments, date, rating, id]
		);
	}

	delete(id) {
		return this.dao.run(
			`DELETE FROM ${table} WHERE id = ?`,
			[id]
		)
	}

	getById(id) {
		return this.dao.get(
			`SELECT * FROM ${table} WHERE id = ?`,
			[id])
	}

	getRatingsByRestoId(resto_id) {
		return this.dao.all(
			`SELECT * FROM ${table} WHERE resto_id = ? ORDER BY rating DESC`,
			[resto_id])
	}

	getAll() {
		return this.dao.all(`SELECT ${table}.*, ${restoTable}.name, ${userTable}.email
		FROM ${table}, ${restoTable}, ${userTable}
		WHERE ${table}.resto_id = ${restoTable}.id AND ${table}.user_id = ${userTable}.id`)
	}
}

module.exports = ReviewRepository;