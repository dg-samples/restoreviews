import {RestoService} from "../services/resto.service";

const express = require('express');
const router = express.Router();
const authorize = require('../_helpers/authorize');
const Role = require('../_helpers/role');

// routes
router.get('/', authorize(), getAll);
router.get('/:id', authorize(), getById);
router.use(authorize(Role.Admin)); // admin for all others
router.post('/', create);
router.put('/:id', updateById);
router.delete('/:id', deleteById);


function create(req, res, next) {
	RestoService.create(req.body.name, req.body.description)
		.then(result => res.json(result))
		.catch(err => next(err));
}

function getAll(req, res, next) {
	RestoService.getAll()
		.then(rows => res.json(rows))
		.catch(err => next(err));
}

function getById(req, res, next) {
	RestoService.getRestoPageReviewsById(req.params.id)
		.then(data => data ?
			res.status(200).json(data) :
			res.sendStatus(404))
		.catch(err => next(err));
}

function updateById(req, res, next) {
	RestoService.updateById(req.params.id, req.body.name, req.body.description)
		.then(result => result ?
			res.status(200).json({message: `Resto with id ${req.params.id} updated!`}) :
			res.status(404).json({message: `Resto not found.`}))
		.catch(err => next(err));
}

function deleteById(req, res, next) {
	RestoService.deleteById(req.params.id)
		.then(result => result ?
			res.status(200).json({message: `Resto with id ${req.params.id} deleted!`}) :
			res.status(404).json({message: `Resto not found.`}))
		.catch(err => next(err));
}

module.exports = router;