import {UserService} from "../services/user.service";

const express = require('express');
const router = express.Router();
const authorize = require('../_helpers/authorize');
const Role = require('../_helpers/role');

// routes
router.post('/authenticate', authenticate);     // public route
// router.get('/refresh', refresh);     // public route
router.post('/signup', signUp);
router.use(authorize(Role.Admin)); // admin for all others
router.post('/', createUser);
router.get('/', getAll);
router.get('/:id', getById);
router.post('/email', getByEmail);
router.put('/:id', updateById);
router.delete('/:id', deleteById);

function authenticate(req, res, next) {
	UserService.authenticate(req.body.email, req.body.password)
		.then(user => user ? res.json(user) : res.status(400).json({message: 'Username or password is incorrect'}))
		.catch(err => next(err));
}

function signUp(req, res, next) {
	UserService.signUp(req.body.email, req.body.password, req.body.role)
		.then(user => res.json(user))
		.catch(err => next(err));
}

function createUser(req, res, next) {
	UserService.createUser(req.body.email, req.body.password, req.body.role)
		.then(user => res.json(user))
		.catch(err => next(err));
}

function getAll(req, res, next) {
	UserService.getAll()
		.then(users => res.json(users))
		.catch(err => next(err));
}

function getById(req, res, next) {
	const currentUser = req.user;
	const id = parseInt(req.params.id);

	// only allow admins to access other user records
	if (id !== currentUser.sub && currentUser.role !== Role.Admin) {
		return res.status(401).json({message: 'Unauthorized'});
	}

	UserService.getById(req.params.id)
		.then(user => user ? res.status(200).json(user) : res.sendStatus(404))
		.catch(err => next(err));
}

function getByEmail(req, res, next) {
	UserService.getByEmail(req.body.email)
		.then(user => user ? res.status(200).json(user) : res.sendStatus(404))
		.catch(err => next(err));
}

function updateById(req, res, next) {
	UserService.updateUserById(req.params.id, req.body.email, req.body.password, req.body.role)
		.then(update => update ?
			res.status(200).json({message: `User with id ${req.params.id} updated!`}) :
			res.status(404).json({message: `User not found.`}))
		.catch(err => next(err));
}

function deleteById(req, res, next) {
	UserService.deleteUserById(req.params.id)
		.then(update => update ?
			res.status(200).json({message: `User with id ${req.params.id} deleted!`}) :
			res.status(404).json({message: `User not found.`}))
		.catch(err => next(err));
}

module.exports = router;