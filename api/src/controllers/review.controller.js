import {ReviewService} from "../services/review.service";

const express = require('express');
const router = express.Router();
const authorize = require('../_helpers/authorize');
const Role = require('../_helpers/role');

// routes
router.post('/', authorize(), create);
router.use(authorize(Role.Admin)); // admin for all others
router.post('/createMock', createMockReview);
router.get('/', getAll);
router.get('/:id', getById);
router.put('/:id', updateById);
router.delete('/:id', deleteById);


function create(req, res, next) {
	ReviewService.create(req.body.resto_id, req.body.comments, req.body.date, req.body.rating, req.user.sub)
		.then(result => res.json(result))
		.catch(err => next(err));
}

function createMockReview(req, res, next) {
	ReviewService.create(req.body.resto_id, req.body.comments, req.body.date, req.body.rating, req.body.user_id)
		.then(result => res.json(result))
		.catch(err => next(err));
}

function getAll(req, res, next) {
	ReviewService.getAll()
		.then(rows => res.json(rows))
		.catch(err => next(err));
}

function getById(req, res, next) {
	ReviewService.getById(req.params.id)
		.then(user => user ? res.status(200).json(user) : res.sendStatus(404))
		.catch(err => next(err));
}

function updateById(req, res, next) {
	ReviewService.updateById(req.params.id, req.body.resto_id, req.body.comments, req.body.date, req.body.rating, req.body.user_id)
		.then(result => result ?
			res.status(200).json({message: `Review with id ${req.params.id} updated!`}) :
			res.status(404).json({message: `Review not found.`}))
		.catch(err => next(err));
}

function deleteById(req, res, next) {
	ReviewService.deleteById(req.params.id)
		.then(result => result ?
			res.status(200).json({message: `Review with id ${req.params.id} deleted!`}) :
			res.status(404).json({message: `Review not found.`}))
		.catch(err => next(err));
}

module.exports = router;