import {reviewRepo} from "../server";
import {UtilityService} from "./utility.service";

export class ReviewService {

	static async create(resto_id, comments, date, rating, user_id) {
		try {
			await reviewRepo.create(resto_id, comments, date, rating, user_id);
			await UtilityService.updateReviews(resto_id);
		} catch (err) {
			return err;
		}
	}

	static async getAll() {
		return await reviewRepo.getAll()
	}

	static async getById(id) {
		return await reviewRepo.getById(id);
	}

	static async updateById(id, resto_id, comments, date, rating, user_id) {
		try {
			let result = await reviewRepo.update(id, resto_id, comments, date, rating, user_id);
			await UtilityService.updateReviews(resto_id);
			return result.changes > 0;
		} catch (err) {
			return err;
		}
	}

	static async deleteById(id) {
		let resto_id = await this.getById(id).then(res => res.resto_id);
		let result = await reviewRepo.delete(id);
		await UtilityService.updateReviews(resto_id);
		return result.changes > 0;
	}
}