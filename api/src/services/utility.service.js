import {restoRepo, reviewRepo} from "../server";

export class UtilityService {
	static async updateReviews(resto_id) {
		try {
			let reviews = await reviewRepo.getRatingsByRestoId(resto_id);
			if (!reviews.length) {
				await restoRepo.updateReviews(resto_id, null, null, null, 0);
				return;
			}
			let top = reviews[0].id;
			let bottom = reviews[reviews.length - 1].id;
			let recent = await this.mostRecentReview(reviews);
			let sum = 0;
			if (reviews.length > 1) {
				sum = reviews.reduce(((previous, current) => previous + current.rating), 0);
			} else {
				sum = reviews[0].rating;
			}
			let avg = sum / reviews.length;
			// console.log("AVG", avg);
			await restoRepo.updateReviews(resto_id, top, bottom, recent, avg);
		} catch (err) {
			console.log(err);
			throw err;
		}
	}

	static async mostRecentReview(reviews) {
		await reviews.forEach(a => {
			a.dateNum = new Date(a.date).getTime();
		});
		await reviews.sort((a, b) => a.dateNum - b.dateNum);
		return reviews[reviews.length - 1].id;
	}
}