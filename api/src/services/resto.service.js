import {restoRepo, reviewRepo} from "../server";

export class RestoService {

	static async create(name, description) {
		return restoRepo.create(name, description);
	}

	static async getAll() {
		return await restoRepo.getAll();
	}

	static async getRestoPageReviewsById(id) {
		let resto = await restoRepo.getById(id);
		let top_review = await reviewRepo.getById(resto.top_review_id);
		let bottom_review = await reviewRepo.getById(resto.bottom_review_id);
		let recent_review = await reviewRepo.getById(resto.recent_review_id);
		return {
			resto: resto,
			top: top_review,
			bottom: bottom_review,
			recent: recent_review
		}
	}

	static async updateById(id, name, description) {
		let result = await restoRepo.update(id, name, description);
		return result.changes > 0;
	}

	static async deleteById(id) {
		let result = await restoRepo.delete(id);
		console.log(result);
		return result.changes > 0;
	}
}