const config = require('../config.json');
const jwt = require('jsonwebtoken');
import {userRepo} from "../server";

const bcrypt = require('bcrypt');
const saltRounds = 10;

export class UserService {
	static async authenticate(email, password) {
		const user = await userRepo.getByEmail(email);
		if (user) {
			let match = false;
			await bcrypt.compare(password, user.password).then(res => match = res);
			if (match) {
				const token = await jwt.sign({sub: user.id, role: user.role}, config.secret);
				const {password, ...userWithoutPassword} = user;
				return {
					...userWithoutPassword,
					token
				};
			} else {
				// wrong password
				return false;
			}
		} else {
			// not found
			return false;
		}
	}

	static async signUp(email, password, role) {
		try {
			let hashPass = "";
			await bcrypt.hash(password, saltRounds).then(hash => hashPass = hash);
			await userRepo.create(email, hashPass, role);
			return await this.authenticate(email, password).then(data => data);
		} catch (err) {
			console.log(err);
			throw err;
		}
	}

	static async createUser(email, password, role) {
		try {
			let hashPass = "";
			await bcrypt.hash(password, saltRounds).then(hash => hashPass = hash);
			return userRepo.create(email, hashPass, role);
		} catch (err) {
			console.log(err);
			throw err;
		}
	}

	static async getAll() {
		return await userRepo.getAll()
	}

	static async getById(id) {
		const user = await userRepo.getById(id);
		if (!user) return false;
		const {password, ...userWithoutPassword} = user;
		return userWithoutPassword;
	}

	static async getByEmail(email) {
		const user = await userRepo.getByEmail(email);
		if (!user) return false;
		const {password, ...userWithoutPassword} = user;
		return userWithoutPassword;
	}

	static async updateUserById(id, email, password, role) {
		try {
			let hashPass = "";
			await bcrypt.hash(password, saltRounds).then(hash => hashPass = hash);
			let result = await userRepo.update(id, email, hashPass, role);
			return !!result.changes;
		} catch (err) {
			console.log(err);
			throw err;
		}
	}

	static async deleteUserById(id) {
		let result = await userRepo.delete(id);
		return !!result.changes;
	}
}